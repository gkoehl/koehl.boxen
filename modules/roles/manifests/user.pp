# == Class: roles::user
#
# 
#
# === Examples
#
# 
#
# === Authors
#
# Gary Koehl <gkoehl@gmail.com>
#
# === Copyright
#
# Copyright 2015 TheKoehl.
#
class roles::user inherits roles {
  package { 'basecamp': provider => 'brewcask' }
  package { 'gimp': provider => 'brewcask' }
  package { 'microsoft-office': provider => 'brewcask' }
  package { 'steam': provider => 'brewcask' }
}