# == Class: roles::dev::heavy
#
# 
#
# === Examples
#
# 
#
# === Authors
#
# Gary Koehl <gkoehl@gmail.com>
#
# === Copyright
#
# Copyright 2015 TheKoehl.
#
class roles::dev::heavy inherits roles::dev {
  # Brewcask packages
  package { 'arduino': provider => 'brewcask' }
  package { 'basecamp': provider => 'brewcask' }
  package { 'boot2docker-status': provider => 'brewcask' }
  package { 'docker-machine': provider => 'brewcask' }
  package { 'terraform': provider => 'brewcask' }
  package { 'jenkins-menu': provider => 'brewcask' }
}