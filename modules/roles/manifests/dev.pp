# == Class: roles::dev
#
# 
#
# === Examples
#
# 
#
# === Authors
#
# Gary Koehl <gkoehl@gmail.com>
#
# === Copyright
#
# Copyright 2015 TheKoehl.
#
class roles::dev inherits roles {
  # Homebrew packages
  package { 'ant':
    ensure => present
  }
  package { 'composer':
    ensure => present
  }
  package { 'docker':
    ensure => present
  }
  package { 'drush':
    ensure => present
  }
  package { 'ffmpeg':
    ensure => present
  }
  package { 'freetype':
    ensure => present
  }
  package { 'gettext':
    ensure => present
  }
  package { 'git':
    ensure => present
  }
  package { 'git-flow':
    ensure => present
  }
  package { 'htop-osx':
    ensure => present
  }
  package { 'hub':
    ensure => present
  }
  package { 'libtool':
    ensure => present
  }
  package { 'node':
    ensure => present
  }
  package { 'openssl':
    ensure => present
  }
  package { 'php56':
    ensure => present
  }
  package { 'puppet':
    ensure => present
  }
  package { 'speech-tools':
    ensure => present
  }
  package { 'tree':
    ensure => present
  }
  package { 'zlib':
    ensure => present
  }

  # Brewcask packages
  package { 'lastpass': provider => 'brewcask' }
  package { 'boot2docker': provider => 'brewcask' }
  package { 'android-studio': provider => 'brewcask' }
  package { 'charles': provider => 'brewcask' }
  package { 'chefdk': provider => 'brewcask' }
  package { 'coda': provider => 'brewcask' }
  package { 'cyberduck': provider => 'brewcask' }
  package { 'intellij-idea-ce': provider => 'brewcask' }
  package { 'jenkins': provider => 'brewcask' }
  package { 'mamp': provider => 'brewcask' }
  package { 'node': provider => 'brewcask' }
  package { 'packer': ensure => present }
  package { 'phpstorm': provider => 'brewcask' }
  package { 'sequel-pro': provider => 'brewcask' }
  package { 'sublime-text': provider => 'brewcask' }
  package { 'sourcetree': provider => 'brewcask' }
  package { 'unity': provider => 'brewcask' }
  package { 'vagrant': provider => 'brewcask' }
  package { 'virtualbox': provider => 'brewcask' }
  package { 'visual-studio-code': provider => 'brewcask' }
  package { 'webstorm': provider => 'brewcask' }
  package { 'wireshark': provider => 'brewcask' }
}