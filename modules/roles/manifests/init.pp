# == Class: roles
#
# 
#
# === Examples
#
# 
#
# === Authors
#
# Gary Koehl <gkoehl@gmail.com>
#
# === Copyright
#
# Copyright 2015 TheKoehl.
#
class roles {
  $my = {
    'phone_number' => '609-560-5855',
  }

  include homebrew
  include brewcask

  # Homebrew packages
  package { 'autojump':
    ensure => present
  }
  # After the install, you will also need to add the following line to your ~/.bash_profile or ~/.zshrc file (and remember to source the file to update your current session):
  # [[ -s `brew --prefix`/etc/autojump.sh ]] && . `brew --prefix`/etc/autojump.sh

  # Brewcask packages
  package { 'adium': provider => 'brewcask' }
  package { 'alfred': provider => 'brewcask' }
  package { 'android-file-transfer': provider => 'brewcask' }
  package { 'audacity': provider => 'brewcask' }
  package { 'chromecast': provider => 'brewcask' }
  package { 'cord': provider => 'brewcask' }
  package { 'daemon-tools-lite': provider => 'brewcask' }
  package { 'divvy': provider => 'brewcask' }
  package { 'dropbox': provider => 'brewcask' }
  package { 'evernote': provider => 'brewcask' }
  package { 'firefox': provider => 'brewcask' }
  package { 'gmail-notifier': provider => 'brewcask' }
  package { 'google-chrome': provider => 'brewcask' }
  package { 'google-drive': provider => 'brewcask' }
  package { 'google-hangouts': provider => 'brewcask' }
  package { 'google-notifier': provider => 'brewcask' }
  package { 'grooveshark': provider => 'brewcask' }
  package { 'handbrake': provider => 'brewcask' }
  package { 'handbrakebatch': provider => 'brewcask' }
  package { 'handbrakecli': provider => 'brewcask' }
  package { 'hipchat': provider => 'brewcask' }
  package { 'kindle': provider => 'brewcask' }
  package { 'minecraft': provider => 'brewcask' }
  package { 'omnigraffle': provider => 'brewcask' }
  package { 'pandora': provider => 'brewcask' }
  package { 'plex-home-theater': provider => 'brewcask' }
  package { 'skype': provider => 'brewcask' }
  package { 'spotify': provider => 'brewcask' }
  package { 'transmit': provider => 'brewcask' }
  package { 'utorrent': provider => 'brewcask' }
  package { 'vlc': provider => 'brewcask' }

  # $home     = "/Users/${::boxen_user}"
  # $my       = "${home}/my"
  # $dotfiles = "${my}/dotfiles"  
  # file { $my:
  #   ensure  => directory
  # }
  # repository { $dotfiles:
  #   source  => 'thekoehl/dotfiles',
  #   require => File[$my]
  # }

  include osx::recovery_message { "If this Mac is found, please call ${my['phone_number']}": }
  include osx::global::enable_keyboard_control_access
  include osx::global::tap_to_click
  include osx::dock::autohide
  include osx::finder::show_external_hard_drives_on_desktop
  include osx::finder::show_removable_media_on_desktop
  include osx::finder::show_hidden_files
  include osx::finder::enable_quicklook_text_selection
  include osx::finder::show_warning_before_emptying_trash
  include osx::finder::show_all_filename_extensions
  include osx::universal_access::ctrl_mod_zoom
  include osx::no_network_dsstores
  include osx::software_update

  include osx::safari::enable_developer_mode

  class { 'osx::global::key_repeat_delay':
    delay => 0
  }
  class { 'osx::global::key_repeat_rate':
    rate => 0
  }
  class { 'osx::global::natural_mouse_scrolling':
    enabled => false
  }
  class { 'osx::dock::pin_position':
    position => 'start'
  }

  # include osx::global::disable_key_press_and_hold
  # include osx::global::enable_standard_function_keys
  # include osx::global::expand_print_dialog
  # include osx::global::expand_save_dialog
  # include osx::global::disable_remote_control_ir_receiver
  # include osx::global::disable_autocorrect
  # include osx::dock::2d
  # include osx::dock::clear_dock
  # include osx::dock::disable
  # include osx::dock::disable_dashboard
  # include osx::dock::dim_hidden_apps
  # include osx::dock::hide_indicator_lights
  # include osx::finder::show_hard_drives_on_desktop
  # include osx::finder::show_mounted_servers_on_desktop
  # include osx::finder::show_all_on_desktop
  # include osx::finder::empty_trash_securely
  # include osx::finder::unhide_library
  # include osx::finder::show_warning_before_changing_an_extension
  # include osx::finder::no_file_extension_warnings
  # include osx::universal_access::enable_scrollwheel_zoom
  # include osx::disable_app_quarantine
  # include osx::keyboard::capslock_to_control

  # class { 'osx::universal_access::cursor_size':
  #   zoom => 2
  # }
  # class { 'osx::dock::icon_size':
  #   size => 36
  # }
  # class { 'osx::dock::position':
  #   position => 'bottom'
  # }
  # osx::dock::hot_corner { 'Top Left':
  #   action => 'Dashboard'
  # }
  # class { 'osx::dock::hot_corners':
  #   top_right => "Start Screen Saver",
  #   bottom_left => "Mission Control"
  # }
  # class { 'osx::mouse::button_mode':
  #   mode => 2
  # }
  # class { 'osx::mouse::smart_zoom':
  #   enabled => true
  # }
  # class { 'osx::mouse::swipe_between_pages':
  #   enabled => true
  # }
  # class { 'osx::dock::magnification':
  #   magnification => true,
  #   magnification_size => 84
  # }
}
